# Como empezar

- Se debe tener un usuario y una contraseña de super-administrador.
- Ingresar al sitio: aparece un formulario que requiere acceso con credenciales.
- Completar el formulario con los datos principales del [club](guia-usuario/club) para diseñar el estilo del sitio.
- Ir a **Ver el sitio** en el menú superior derecho.
- Entramos a la configuración del sitio.
	- Elegir el club recientemente grabado.
	- Un título para el sitio: será el que aparezca en los buscadores.
	- **Slider** es una caracteristica que permite poner varias imágenes en el encabezado de la página principal en forma de *carousel*. Dejarlo sin activar ya que será algo que veas después.
	- Copiar los enlaces a las redes sociales. Esto desplegará los accesos en el pié de página.
	- Grabar
- Ir a **Ver el sitio** en el menú superior derecho.
- Los siguientes pasos podrían ser:
	- Agregar [actividades](guia-usuario/actividades)
	- Agregar [noticias](guia-usuario/noticias)
	- Agregar [cultura](guia-usuario/cultura)
